import checkers.inference.reim.quals.*;

class Date {
  int hours;
  void setHours(@Readonly Date this, int hours) {
    this.hours = hours;
  }
}

class DateCell {
  @Readonly Date date;
  @Readonly Date getDate(@Readonly DateCell this) {
    return this.date;
  }
  void cellSetHours(@Readonly DateCell this) {
    @Readonly Date md = this.getDate();
    md.setHours(1);
  }
}

class Main {
  public static final void main(String args[]) {
    new DateCell().cellSetHours();
  }
}

